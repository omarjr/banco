
public class ContaPoupanca extends Conta {
	
	String lucro;

	public ContaPoupanca(String numero, Cliente dadosPessoais, String lucro) {
		super(numero, dadosPessoais);
		this.lucro = lucro;
	}

	public String getLucro() {
		return lucro;
	}

	public void setLucro(String lucro) {
		this.lucro = lucro;
	}

}
