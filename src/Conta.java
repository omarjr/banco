
public class Conta {
	
	private String numero;
	private String ngencia;
	private Cliente dadosPessoais;
	private String dataAbertura;
	private String saldo;
	
	public Conta(String numero, Cliente dadosPessoais) {
		this.numero = numero;
		this.dadosPessoais = dadosPessoais;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNgencia() {
		return ngencia;
	}

	public void setNgencia(String ngencia) {
		this.ngencia = ngencia;
	}

	public Cliente getDadosPessoais() {
		return dadosPessoais;
	}

	public void setDadosPessoais(Cliente dadosPessoais) {
		this.dadosPessoais = dadosPessoais;
	}

	public String getDataAbertura() {
		return dataAbertura;
	}

	public void setDataAbertura(String dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	public String getSaldo() {
		return saldo;
	}

	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}
	
	}
