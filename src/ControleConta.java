
public class ControleConta {
	
	public void sacarQuantia(Conta umaConta, int quantiaRetirada){
		int novoSaldo = Integer.parseInt(umaConta.getSaldo()) - quantiaRetirada;
		umaConta.setSaldo(Integer.toString(novoSaldo));
	}
	
	public void depositarQuantia(Conta umaConta, int quantiaDepositada) {
		int novoSaldo = Integer.parseInt(umaConta.getSaldo()) + quantiaDepositada;
		umaConta.setSaldo(Integer.toString(novoSaldo));
	}

}
