

public class ContaCorrente extends Conta {
	
	private String cartaoDeCredito;
	private String cartaoDeDebito;
	
	public ContaCorrente(String numero, Cliente dadosPessoais, String cartaoDeCredito, String cartaoDeDebito) {
		super(numero, dadosPessoais);
		this.cartaoDeCredito = cartaoDeCredito;
		this.cartaoDeDebito = cartaoDeDebito;
	}

	public String getCartaoDeCredito() {
		return cartaoDeCredito;
	}

	public void setCartaoDeCredito(String cartaoDeCredito) {
		this.cartaoDeCredito = cartaoDeCredito;
	}

	public String getCartaoDeDebito() {
		return cartaoDeDebito;
	}

	public void setCartaoDeDebito(String cartaoDeDebito) {
		this.cartaoDeDebito = cartaoDeDebito;
	}

}
